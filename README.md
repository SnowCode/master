# Master project
> This project is still in beta, if you want a better password generator go checkout [Master Password](https://masterpassword.app/)

The **Master project** is a password generator easy to understand and educational based on [Master Password](https://masterpassword.app/). 
It is available for all platform and is made to be intuitive. 

## Installation
### Installation for your browser
1. Install [Violent Monkey](https://violentmonkey.github.io/) or similar as extension for your browser.
2. Click [here](https://codeberg.org/SnowCode/master/raw/branch/master/script.user.js) and click on `Confirm Installation`
3. Click on the extension's icon, next to *Master Browser*, click on the code symbol `</>` 
4. Change the `username` value to what ever you want (it needs to remains the same over time)
5. Click on `Save`.

> You can also change the salt if you want an extra layer of security, but you will have to change these 2 values on every installations / devices.

### Installation for computer
> There is actually no real install, but there is a python script that you can run

1. Download [this file](./master.py) on your computer
2. Edit the file and change the `username` value to what ever you want (it needs to remains the same over time)
3. Make sure you have Python3 installed on your computer.
4. Run the file using Python. You can also do it using the following command:

```shell
python3 master.py
```

> You can also change the salt if you want an extra layer of security, but you will have to change these 2 values on every installations / devices.

### Installation for Android
1. Go into the Android settings > Security > Allow install from unkown sources
2. Download the file [here](https://gitea.com/chopin42/master/raw/branch/master/mobile/master-0.1-armeabi-v7a-debug.apk)
3. Open it and click on `Install`

## Usage on your browser
This is an example on how to login on websites:

![demo1](./docs/demo1.gif)

1. Go on the username's field and press `ALT` + `J` then `CTRL` + `V` to type the username.
2. Go on the password field and write your **master password**
3. Press `ALT` + `G` then `CTRL` + `V` to paste the password

Generate a password for another url (example generate the password of `www.reddit.com` on `old.reddit.com`)

![demo2](./docs/demo2.gif)

1. Go on the username's field and press `ALT` + `J` then `CTRL` + `V` to type the username.
2. Go on the password field and write your **master password**
3. Press `ALT` + `U`, write the original url (here it is `www.reddit.com`), then press `CTRL` + `V` to paste the password.

## License
This project is under GNU-GPL-v3. You can find more informations in the [LICENSE file](./LICENSE).
