#!/usr/bin/python3
from getpass import getpass
from base64 import b64encode as b64
from hashlib import sha512
from pyperclip import copy

# Get the variables
username = "julien.lejoly2712@gmail.com"
url = input("Website > ")
master = getpass("Password > ")

# Generate the password
password = str(b64(bytes(sha512(bytes(username + url + master, "utf-8")).hexdigest(), "utf-8"))).split("'")[1][-20:]

# Copy the password
copy(password)
print("Password copied to clipboard")
