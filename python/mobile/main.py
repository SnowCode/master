from kivy.app import App
from kivy.lang import Builder
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.config import ConfigParser
from kivy.core.clipboard import Clipboard
from hashlib import sha512
from base64 import b64encode as b64

config = ConfigParser()
config.read("myconfig.ini")
config.setdefaults("main", {"username": ""})

Builder.load_file("app.kv")

class MainScreen(Screen):
    def generate(self):
        username = config.get("main", "username")
        master = self.ids.psw_field.text
        url = self.ids.website_field.text

        password = str(b64(bytes(sha512(bytes(username + url + master, "utf-8")).hexdigest(), "utf-8"))).split("'")[1][-20:]
        Clipboard.copy(password)

        self.ids.website_field.text = ''
        self.ids.psw_field.text = ''

sm = ScreenManager()
sm.add_widget(MainScreen(name="main"))

class MainApp(App):
    def build(self):
        self.use_kivy_settings = False
        return sm

    def build_settings(self, settings):
        settings.add_json_panel(
            "Settings",
            config,
            data=open("settings.json").read(),
        )

if __name__ == "__main__":
    MainApp().run()
