// ==UserScript==
// @name           Master-Browser
// @description    A sync-less password manager in your browser!
// @version        v0.4
// @include        *
// @grant          GM_setClipboard
// @require        https://raw.githubusercontent.com/emn178/js-sha512/master/src/sha512.js
// ==/UserScript==


function gen(username, url) {
  var master = document.activeElement.value
  var password = btoa(sha512(username + url + master))
  document.activeElement.value = ""
  GM_setClipboard(password.slice(password.length - 20))
}


(function() {
  var username = "<your username here>"
  document.addEventListener('keydown', function(e) {
    if (e.keyCode == 74 && e.altKey) {
      console.log("Copying username")
      GM_setClipboard(username)
    }

    if (e.keyCode == 71 && e.altKey) {
      var url = document.URL.split("://")[1].split("/")[0]
      gen(username, url)
    }

    if (e.keyCode == 85 && e.altKey) {
      var url = prompt("What is the url (include 'www.')")
      gen(username, url)
    }
  }, false)
})()
